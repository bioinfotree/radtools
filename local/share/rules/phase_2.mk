### phase_2.mk --- 
## 
## Filename: phase_2.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Jan 18 15:10:52 2012 (+0100)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


 extern ../phase_1/naccari_family_full.markers as FAM_FULL_MARKER
 extern ../phase_1/stursnps_pops_full.markers as POP_FULL_MARKER


$(notdir $(FAM_FULL_MARKER)): $(FAM_FULL_MARKER)
	ln -sf $< $@

$(notdir $(POP_FULL_MARKER)): $(POP_FULL_MARKER) 
	ln -sf $< $@


baeri_lena_vs_ob.markers: $(notdir $(POP_FULL_MARKER))
	segr_filter_2 --p1_pn 7 9 --p2_pn 10  12 --p1_thr 2 0 --p2_thr 2 0 < $< > $@ 2>$(addsuffix .stat, $(basename $@))

persicus_gualdenstaedtii_nord_caspian.markers: $(notdir $(POP_FULL_MARKER))
	segr_filter_2 --p1_pn 1 2 --p2_pn 3  4 --p1_thr 2 0 --p2_thr 2 0 < $< > $@ 2>$(addsuffix .stat, $(basename $@))

gueldenstaedtii_azov_vs_caspian.markers: $(notdir $(POP_FULL_MARKER))
	segr_filter_2 --p1_pn 3 4 --p2_pn 5  6 --p1_thr 2 0 --p2_thr 2 0 < $< > $@ 2>$(addsuffix .stat, $(basename $@))



op%_naccari_family_2allelic.markers: $(notdir $(FAM_FULL_MARKER))
	segr_filter_2 --p1_pn 1 3 --p2_pn 4 6  --p3_pn 7 12  --p1_thr 2 0 --p2_thr 2 0 --p3_thr $* 0 < $< > $@ 2>$(addsuffix .stat, $(basename $@))


.PHONY: test
test:
	echo $(STD_ERR)



# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += baeri_lena_vs_ob.markers \
	 persicus_gualdenstaedtii_nord_caspian.markers \
	 gueldenstaedtii_azov_vs_caspian.markers \
	 op5_naccari_family_2allelic.markers \
	 op4_naccari_family_2allelic.markers \
	 op3_naccari_family_2allelic.markers


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += 

# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -rf *.markers \
	$(RM) -rf *.stat


######################################################################
### phase_2.mk ends here
