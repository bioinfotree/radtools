### phase_2.mk --- 
## 
## Filename: phase_2.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Thu Sep  1 09:59:38 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# sam alignment
extern ../phase_4/radtags_aln.sam as RADTAGS_ALN_SAM
extern ../phase_4/radtags.fasta as RADTAGS_FASTA


assembly_ln.fasta: $(ASSEMBLY)
	ln -sf $(ASSEMBLY) $@



# index reference sequence
assembly_ln.fasta.fai: assembly_ln.fasta
	samtools faidx $<

# output alignments with MAPQ(uality) >= 1 in bam format
radtags_aln.bam: assembly_ln.fasta.fai $(RADTAGS_ALN_SAM)
	samtools view -uq1 -t $< $^2 -o $@

# sort alignments by leftmost coordinates
radtags_aln_sorted.bam: radtags_aln.bam
	samtools sort -o $< out.prefix > $@

# index sorted alignment for fast random access
radtags_aln_sorted.bai: radtags_aln_sorted.bam
	samtools index $< ;
	mv $(addsuffix .bai,$<) $@

# return a tab delimited file with:
# contig length aligned_reads not_aligned_reads
# filters line with aligned_reads > 0
radtags_aln_tab.txt: radtags_aln_sorted.bam radtags_aln_sorted.bai
	samtools idxstats $< | \
	bawk '!/^[$$,\#+]/ { \
	split($$0,a,"\t"); \
	if (a[3] > 0) \
	{ \
	print $$0; \
	} \
	}' > $@

# call SNPs and short INDELs. Save in BCF (binary variant call format)
radtags_var_raw.bcf: assembly_ln.fasta radtags_aln_sorted.bam
	samtools mpileup -uf $< $^2 | bcftools view -bvcg - > $@

# convert BCF to VCF. Sets the maximum read depth to call a SNP to 5 (-D)
radtags_var_flt.vcf: radtags_var_raw.bcf
	bcftools view $< | vcfutils varFilter -D5 > $@

# convert from VCF format to tab-delimited text file 
radtags_var_flt_tab.txt: radtags_var_flt.vcf
	_comment () { echo -n ""; }; \
	_comment "env var needed by vcf-to-tab"; \
	PERL5LIB=${PERL5LIB}:${HOME}/bioinfotree/binary/Michele-Desktop/local/lib \
	export PERL5LIB; \
	vcf-to-tab < $< > $@


# prepare summary table
radtags_aln_tab_summary.txt: radtags_aln_tab.txt assembly_ln.fasta $(RADTAGS_FASTA)
	_comment () { echo -n ""; }; \
	CONTIGS_COUNT=`fasta_count $^2`; \
	TAGS_COUNT=`fasta_count $^3`; \
	_comment "get overview"; \
	bawk -v contigs_count=$$CONTIGS_COUNT -v tags_count=$$TAGS_COUNT ' \
	BEGIN { print "aligned_contigs\taligned_tags\tfrac_aligned_contigs\tfrac_aligned_tags";} \
	!/^[$$,\#+]/ { \
	split($$0,a,"\t"); \
	tags = tags + a[3]; \
	} END { \
	frac_tags = tags/tags_count*100; \
	frac_contigs = NR/contigs_count*100; \
	printf "%i\t%i\t%.2f\t%.2f\n", NR, tags, frac_contigs, frac_tags; \
	} ' $< > $@




# >>>>>>>>>>>>>>>>>>>>>>> ALL NEED FOR RUNNING rescan_seq <<<<<<<<<<<<<<<<<<<<<<<

# Installation instructions:
# http://code.google.com/p/biopieces/wiki/Installation

# Modify the below paths according to your settings.
# If you have followed the installation instructions step-by-step
# the below should work just fine.

# Directory where biopieces are installed
BP_DIR=${HOME}/bioinfotree/binary/Michele-Desktop/local/stow/biopieces
# Contains genomic data etc.
BP_DATA=${PWD}/BP_DATA
# Required temporary directory.
BP_TMP=${PWD}/tmp
# Required log directory.
BP_LOG=${PWD}/BP_LOG
export BP_DIR 
export BP_DATA
export BP_TMP
export BP_LOG
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

# rescan_seq uses information from REBASE to locate restriction enzyme (RE)
# binding sites.

# The positions reported as values to the RE_MATCHES key denotes the beginning
# postion of the binding site (0-based), not the cleavage site - which
# may be outside the binding site.
restr_sites-fake_lst.txt: assembly_ln.fasta
	_comment () { echo -n ""; }; \
	_comment "NEED FOR RUNNING rescan_seq"; \
	mkdir -p $(BP_DATA) $(BP_TMP) $(BP_LOG); \
	if [ -f "$(BP_DIR)/bp_conf/bashrc" ]; then \
	source "$(BP_DIR)/bp_conf/bashrc"; \
	fi; \
	_comment ">>>>>>>>>>>>>>>>>>>>>>>>>>>"; \
	rescan_seq_provider < $^ | rescan_seq --res_enz SbfI > $@



# Since my contigs have repeats masked with "XX .."
# the program rescan_seq found subsequent restriction sites 
# along the whole sequence of "X". These fake restriction sites
# are present in this table
restr_sites-fake_tab.txt: restr_sites-fake_lst.txt
	filt_rsites_tab $^ > $@


# Search NON-OVERLAPPING restriction sites of an enzyme specified and its
# reverse complement
# SbfI=CCTGCAGG
restr_sites_tab.txt: assembly_ln.fasta
	rsite_finder --r_site CCTGCAGG < $< > $@


RADTAGS_ALN_TAB_REST=radtags_aln_tab-restr_sites_tab.txt
radtags_aln-restr_sites_tab.txt: radtags_aln_tab.txt restr_sites_tab.txt
	joins_rsites-aln --tabs $< $^2 --rest_file $(RADTAGS_ALN_TAB_REST) > $@


RADTAGS_ALN_TAB_REST-FAKE=radtags_aln_tab-restr_sites-fake_tab.txt
radtags_aln-restr_sites-fake_tab.txt: radtags_aln_tab.txt restr_sites-fake_tab.txt
	joins_rsites-aln --tabs $< $^2 --rest_file $(RADTAGS_ALN_TAB_REST-FAKE) > $@







# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += assembly_ln.fasta \
	 assembly_ln.fasta.fai \
	 radtags_aln.bam \
	 radtags_aln_sorted.bam \
	 radtags_aln_sorted.bai \
	 radtags_aln_tab.txt \
	 radtags_var_raw.bcf \
	 radtags_var_flt.vcf \
	 radtags_var_flt_tab.txt \
	 radtags_aln_tab_summary.txt \
	 restr_sites-fake_lst.txt \
	 restr_sites-fake_tab.txt \
	 restr_sites_tab.txt \
	 radtags_aln-restr_sites_tab.txt \
	 radtags_aln-restr_sites-fake_tab.txt


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += assembly_ln.fasta \
	 assembly_ln.fasta.fai \
	 radtags_aln.bam \
	 radtags_aln_sorted.bam \
	 radtags_aln_sorted.bai \
	 radtags_aln_tab.txt \
	 radtags_var_raw.bcf \
	 radtags_var_flt.vcf \
	 radtags_var_flt_tab.txt \
	 radtags_aln_tab_summary.txt \
	 restr_sites-fake_lst.txt \
	 restr_sites-fake_tab.txt \
	 restr_sites_tab.txt \
	 radtags_aln-restr_sites_tab.txt \
	 $(RADTAGS_ALN_TAB_REST) \
	 $(RADTAGS_ALN_TAB_REST-FAKE) \
	 radtags_aln-restr_sites-fake_tab.txt


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -rf $(BP_DATA) $(BP_TMP) $(BP_LOG)


######################################################################
### phase_2.mk ends here
