### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


RAW_ILLUMINA_FASTQ_1 ?=
#
RAW_ILLUMINA_FASTQ_2 ?=
#
BARCODES_TAB ?=
#
CPUS ?= 4
#
TEST ?= true
#
RADTAGS-RADTAGS_OPTS ?=
#
RADTAGS-RADTAGS_CLUSTER_DISTANCE ?= --cluster_distance 5


# create log dir
log:
	mkdir -p $@;



raw_illumina_fastq_1_ln.fastq: $(RAW_ILLUMINA_FASTQ_1)
	ln -sf $< $@

raw_illumina_fastq_2_ln.fastq: $(RAW_ILLUMINA_FASTQ_2)
	ln -sf $< $@

# produces pools file
BARCODES = sturgeons.pools
$(BARCODES): $(BARCODES_TAB)
	bawk ' \
	!/^[$$,\#+]/ { \
	split($$0,a,"\t"); \
	split(a[3],b,","); \
	for (i in b) \
	{ \
	if (b[i] != "-") \
	{ \
	printf "%s_%s_%i\t%s\n", a[1], a[2], i, b[i]; \
	} \
	} \
	}  ' $< > $@


# get chunks of the original fastq file for testing purpose
raw_chunk_1.fastq: raw_illumina_fastq_1_ln.fastq 
	get_fastq @1000:50000 <$< > $@

raw_chunk_2.fastq: raw_illumina_fastq_2_ln.fastq 
	get_fastq @1000:50000 <$< > $@


POOLS_DIR = $(basename $(BARCODES))



# test variable value
ifeq ($(TEST),true)
# for testing purpose
$(POOLS_DIR): raw_chunk_1.fastq raw_chunk_2.fastq $(BARCODES) log
	RADpools --in $< --paired $^2 --max_processes $(CPUS) --verbose --directory $@ &> ./$^4/radpools.log
else
$(POOLS_DIR): raw_illumina_fastq_1_ln.fastq raw_illumina_fastq_2_ln.fastq $(BARCODES) log &> ./$^4/radpools.log
	RADpools --in $< --paired $^2 --max_processes $(CPUS) --verbose --directory $@
endif



.Phony: RADtags
RADtags: $(POOLS_DIR) log
	RADtags --directory $< --site TGCAGG --max_processes $(CPUS) --read_threshold 3 --verbose  $(RADTAGS-RADTAGS_CLUSTER_DISTANCE) $(RADTAGS-RADTAGS_OPTS) &> ./$^2/radtags.log


STURGEONS_FULL_MARKERS = sturgeons_full.markers
$(STURGEONS_FULL_MARKERS): $(POOLS_DIR) $(BARCODES) RADtags log
	RADmarkers --directory $(POOLS_DIR) --snpsout --qualsout --verbose --fragments --mismatches 3 --tag_count_threshold 3 > $@ 2> ./$^4/radmarkers.sturgeons.log

# RADmarkers output in the old format.
STURGEONS_FULL_MARKERS_OLD = $(addsuffix .old, $(STURGEONS_FULL_MARKERS))
$(STURGEONS_FULL_MARKERS_OLD): $(POOLS_DIR) $(BARCODES) RADtags
	RADmarkers --directory $(DEST_DIR_AN) --snpsout --qualsout --verbose --fragments --mismatches 3 --tag_count_threshold 3 --old_output > $@




DEST_DIR_AN = AN
naccari_family_full.markers: $(POOLS_DIR) $(BARCODES) RADtags log
	_comment () { echo -n ""; }; \
	mkdir -p $(DEST_DIR_AN); \
	cd $(DEST_DIR_AN); \
	ln -sf ../$</AN_* . -v; \
	cd ..; \
	bawk ' \
	# get only lines with AN_ \
	/^AN_*/ { \
	print $$0; \
	} ' $^2 > $(DEST_DIR_AN).pools; \
	_comment "options --snpsout --qualsout options only work in with option --old_output"; \
	RADmarkers --directory $(DEST_DIR_AN) --snpsout --qualsout --verbose --fragments --mismatches 3 --tag_count_threshold 3 > $@ 2> ./$^4/radmarkers.naccarii_family.log

# RADmarkers output in the old format.
NACCARI_FAMILY_FULL_MARKERS_OLD = $(addsuffix .old, naccari_family_full.markers)
$(NACCARI_FAMILY_FULL_MARKERS_OLD): naccari_family_full.markers
	RADmarkers --directory $(DEST_DIR_AN) --snpsout --qualsout --verbose --fragments --mismatches 3 --tag_count_threshold 3 --old_output > $@





DEST_DIR_POPS = POPS
stursnps_pops_full.markers: $(POOLS_DIR) $(BARCODES) RADtags log
	mkdir -p $(DEST_DIR_POPS); \
	cd $(DEST_DIR_POPS); \
	ln -sf ../$</Stur-Pop* . -v; \
	cd ..; \
	bawk ' \
	# get only lines with Stur-Pop \
	/^Stur-Pop*/ { \
	print $$0; \
	} ' $^2 > $(DEST_DIR_POPS).pools; \
	RADmarkers --directory $(DEST_DIR_POPS) --snpsout --qualsout --verbose --fragments --mismatches 3 --tag_count_threshold 3 > $@ 2> ./$^4/radmarkers.pops.log

# RADmarkers output in the old format.
STURSNPS_POPS_FULL_MARKERS_OLD = $(addsuffix .old, stursnps_pops_full.markers)
$(STURSNPS_POPS_FULL_MARKERS_OLD): stursnps_pops_full.markers
	RADmarkers --directory $(DEST_DIR_POPS) --snpsout --qualsout --verbose --fragments --mismatches 3 --tag_count_threshold 3 --old_output > $@




# op%_naccari_family_2allelic.markers: naccari_family_full.markers 
# 	segr_filter --Op $* < $< > $@ 2>$(addsuffix .stat, $(basename $@))







.PHONY: test
test:
	echo $(STD_ERR)



# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += raw_illumina_fastq_1_ln.fastq \
	 raw_illumina_fastq_2_ln.fastq \
	 $(BARCODES) \
	 $(POOLS_DIR) \
	 RADtags \
	 naccari_family_full.markers \
	 stursnps_pops_full.markers \
	 #sturgeons_full.markers \
	 $(STURGEONS_FULL_MARKERS_OLD) \
	 $(NACCARI_FAMILY_FULL_MARKERS_OLD) \
	 $(STURSNPS_POPS_FULL_MARKERS_OLD) \

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += raw_illumina_fastq_1_ln.fastq \
	 raw_illumina_fastq_2_ln.fastq \
	 $(BARCODES) \
	 raw_chunk_1.fastq \
	 raw_chunk_2.fastq \
	 $(DEST_DIR_AN).pools \
	 $(DEST_DIR_POPS).pools \
	 $(NACCARI_FAMILY_FULL_MARKERS_OLD) \
	 $(STURSNPS_POPS_FULL_MARKERS_OLD) \
	 $(STD_ERR) \
	 $(STURGEONS_FULL_MARKERS_OLD)


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -rf log \
	$(RM) -rf $(POOLS_DIR) \
	$(RM) -rf $(DEST_DIR_AN) \
	$(RM) -rf $(DEST_DIR_POPS) \
	$(RM) -rf *.markers \
	$(RM) -rf *.stat


######################################################################
### phase_1.mk ends here
