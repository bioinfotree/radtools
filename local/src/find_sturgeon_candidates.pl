#!/usr/bin/env perl

# find_sturgeon_candidates.pl
#
# Look for ambiguous segregation patterns in Rob Ogden's SturSNP data
# SturPops are this:
# Pop1-1
# Pop1-2
# Pop2-1
# Pop2-2
# Pop3-1
# Pop3-2
# Pop4-1
# Pop4-2
# Pop4-3
# Pop5-1
# Pop5-2
# Pop5-3
#
# Want to find differences in Pops 4 and 5; ideally, a SNP would be present
# in all three technical replicates, but because of variation in coverage,
# it is likely that there will be missing tags in a replicate.
#
# So look for any 2 out of 3 samples in a pop, or all 3, vs no samples in
# the other pop.
# Start with biallelic clusters only.
#
# Input is RADmarkers format.
#
# Author: John Davey john.davey@ed.ac.uk
# Begun 4/08/11

#############################################################################
###                                                                       ###
###                                 CODE                                  ###
###                                                                       ###
#############################################################################

use strict;
use warnings;
use Carp;
use English;
use Getopt::Long;
use Data::Dumper;

# Autoflush output so reporting on progress works
$| = 1;

my $in_filename = "";
my $comparison  = "";
my $options_okay =
  GetOptions( 'input=s' => \$in_filename, 'comparison=s' => \$comparison, );

croak "\nUsage: perl find_sturgeon_candidates.pl -i input_file -c comparison\n"
  if !$options_okay;

croak
"Please specify a comparison (naccari, compare1and2, compare2and3, compare4and5)\n"
  if ( $comparison eq "" );

open my $in_file, '<', $in_filename
  or croak "Can't open $in_filename: $OS_ERROR!\n";

my $header = <$in_file>;
print $header;

chomp $header;
my @samples = split /\t/, $header;
shift @samples;    # ClusterID
shift @samples;    # ClusterTags
shift @samples;    # SegPattern
shift @samples;    # Tag

my %patterns;
my %cluster_lines;
my %sequences;

while ( my $allele_line = <$in_file> ) {
    chomp $allele_line;
    my @fields = split /\t/, $allele_line;
    push @{ $patterns{ $fields[0] } },      $fields[2];
    push @{ $cluster_lines{ $fields[0] } }, $allele_line;
    push @{ $sequences{ $fields[0] } },     $fields[3];
}
close $in_file;

foreach my $cluster_id ( sort { $a <=> $b } keys %patterns ) {

    if ( $comparison eq "compare1and2" ) {
        compare1and2( $cluster_id, \%patterns, \%cluster_lines, \%sequences );
    }

    if ( $comparison eq "compare12and3" ) {

        compare12and3( $cluster_id, \%patterns, \%cluster_lines, \%sequences );
    }
    if ( $comparison eq "compare2and3" ) {
        compare2and3( $cluster_id, \%patterns, \%cluster_lines, \%sequences );
    }
    if ( $comparison eq "compare4and5" ) {

        compare4and5( $cluster_id, \%patterns, \%cluster_lines, \%sequences );
    }

    if ( $comparison eq "naccari" ) {
        comparenaccari( $cluster_id, \%patterns, \%cluster_lines, \%sequences );
    }
}

sub compare1and2 {
    my ( $cluster_id, $pattern_ref, $cluster_line_ref, $sequence_ref ) = @_;

    return if (@{$pattern_ref->{$cluster_id}} != 2 );

    # Pattern 1 is absent in pop1, present in pop2;
    # pattern 2 is present in pop1, absent in pop2
    if (    ( $pattern_ref->{$cluster_id}[0] =~ /--11[1-]{8}/ )
        and ( $pattern_ref->{$cluster_id}[1] =~ /11--[1-]{8}/ ) )
    {
        print_allele( $cluster_id, $cluster_line_ref, $sequence_ref );
    }
    return;
}

sub compare12and3 {
    my ( $cluster_id, $pattern_ref, $cluster_line_ref, $sequence_ref ) = @_;

    return if (@{$pattern_ref->{$cluster_id}} != 2 );
    
    
    if (    ( $pattern_ref->{$cluster_id}[0] =~ /----11[1-]{6}/ )
        and ( $pattern_ref->{$cluster_id}[1] =~ /1111--[1-]{6}/ ) )
    {
        print_allele( $cluster_id, $cluster_line_ref, $sequence_ref );
    }
    return;
}

sub compare2and3 {
    my ( $cluster_id, $pattern_ref, $cluster_line_ref, $sequence_ref ) = @_;

    return if (@{$pattern_ref->{$cluster_id}} != 2 );
    
    my @patterns;
    push @patterns, substr( $pattern_ref->{$cluster_id}[0], 2 );
    push @patterns, substr( $pattern_ref->{$cluster_id}[1], 2 );
    my ( $pattern1, $pattern2 ) = sort @patterns;

    if (    ( $pattern1 =~ /--11[1-]{6}/ )
        and ( $pattern2 =~ /11--[1-]{6}/ ) )
    {
        print_allele( $cluster_id, $cluster_line_ref, $sequence_ref );
    }
    return;
}

sub compare4and5 {
    my ( $cluster_id, $pattern_ref, $cluster_line_ref, $sequence_ref ) = @_;

    return if (@{$pattern_ref->{$cluster_id}} != 2 );
    
    my @patterns;
    push @patterns, substr( $pattern_ref->{$cluster_id}[0], 6 );
    push @patterns, substr( $pattern_ref->{$cluster_id}[1], 6 );
    my ( $pattern1, $pattern2 ) = sort @patterns;

    # Pattern 1 is absent in pop 4, present in pop 5
    return if ( $pattern1 !~ /---[1-]{3}/ );
    return if pop_allele_absent( substr $pattern1, 3, 3 );

    # Pattern 2 is present in pop 4, absent in pop 5
    return if ( $pattern2 !~ /[1-]{3}---/ );
    return if pop_allele_absent( substr $pattern2, 0, 3 );

    print_allele( $cluster_id, $cluster_line_ref, $sequence_ref );
}

sub comparenaccari {
    my ( $cluster_id, $pattern_ref, $cluster_line_ref, $sequence_ref ) = @_;

    return if (@{$pattern_ref->{$cluster_id}} != 2 );
    
    my $pattern1 = $pattern_ref->{$cluster_id}[0];
    my $pattern2 = $pattern_ref->{$cluster_id}[1];
    
    # Pattern 1 is absent in parent 1, present in parent 2
    return if ( $pattern1 !~ /^---[1-]{3}/ );
    return if pop_allele_absent( substr $pattern1, 3, 3 );

    # Pattern 2 is present in parent 1, absent in parent 2
    return if ( $pattern2 !~ /^[1-]{3}---/ );
    return if pop_allele_absent( substr $pattern2, 0, 3 );

    my $offspring1 = substr ($pattern_ref->{$cluster_id}[0], 6);
    my $offspring2 = substr ($pattern_ref->{$cluster_id}[1], 6);

    $offspring2 =~ tr/\-1/1\-/;
    
    return if ($offspring1 ne $offspring2);

    print_allele( $cluster_id, $cluster_line_ref, $sequence_ref );


    return;
}

sub print_allele {
    my ( $cluster_id, $cluster_line_ref, $sequence_ref ) = @_;
    print
"$cluster_line_ref->{$cluster_id}[0]\n$cluster_line_ref->{$cluster_id}[1]\n";

    my @seq1 = split //, $sequence_ref->{$cluster_id}[0];
    my @seq2 = split //, $sequence_ref->{$cluster_id}[1];
    my $snp_line = "";

    foreach my $i ( 0 .. $#seq1 ) {
        $snp_line .= ( $seq1[$i] eq $seq2[$i] ) ? " " : "*";
    }
    print "\t\t\t\t$snp_line\n\n";

    return;
}

sub pop_allele_absent {
    my ($pattern) = @_;
    return 1 if ( $pattern eq "1--" );
    return 1 if ( $pattern eq "-1-" );
    return 1 if ( $pattern eq "--1" );
    return 1 if ( $pattern eq "---" );
    return 0;
}
