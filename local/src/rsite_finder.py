#!/usr/bin/env python
# rsite_finder.py ---
# 
# Filename: rsite_finder.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Tue Sep 27 10:27:06 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
import sys
#
from  Bio.SeqIO import FastaIO
#
from Bio.Seq import Seq
#
import argparse
#
import re


def main(arguments):

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()
    # SbfI restictio site
    # http://www.neb.com/nebecomm/products/productR0642.asp
    r_site = Seq(args.r_site)
    # reverse complement sequence
    r_site_comp = r_site.reverse_complement()
    
    # use pattern matching
    site_pattern = re.compile(str(r_site))
    site_pattern_comp = re.compile(str(r_site_comp))
    
    # header
    print "#\tsequence_name\tsequence_length\trestriction_sites_count\trecognition_site_starting_points"
    
    # use biopython fasta iterator; returns seqrecord obj
    for record in FastaIO.FastaIterator(sys.stdin):
        finds = site_match(str(record.seq), site_pattern, site_pattern_comp)
        if len(finds) > 0:
            sys.stdout.write("%s\t%i\t%i\t%s\n" % (record.id, len(record.seq), \
                                               len(finds), ";".join(finds)))
            # for item in finds:
            #     sys.stdout.write("%i;" % (item[0]))
            # sys.stdout.write("\n")
    
    return 0




# def site_match(line, pattern):
#     """
#     Return all NON-OVERLAPPING matches of pattern in string
#     """
#     finds = []
#     if pattern.search(line):
#         # iterator over matches
#         for m in pattern.finditer(line):
#             finds.append((m.start(), m.end(), m.group(0)))
            
#     return finds



def site_match(line, pattern, pattern_comp):
    """
    Return all NON-OVERLAPPING matches of pattern in string
    and its reverse complement
    """
    finds = []
    if pattern.search(line):
        # iterator over matches
        for m in pattern.finditer(line):
            finds.append(str(m.start()))

    # search reverse complement anly if the enzyme is not palindromic
    if pattern_comp.search(line) and (pattern != pattern_comp):
        # iterator over matches
        for m in pattern_comp.finditer(line):
            finds.append(str(m.start()))
        
    return finds





def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Search NON-OVERLAPPING restriction sites of an enzyme specified and its complement, in a FASTA file from std-in (<)")
    parser.add_argument("--r_site", "-r", dest="r_site", required=False, type=str, default=r"CCTGCAGG", help="Restriction site of the enzyme to search for. Default CCTGCAGG (SbfI)")

    args = parser.parse_args()
    
    return args



# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)



# 
# rsite_finder.py ends here
