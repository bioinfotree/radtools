#!/usr/bin/env python

# test_fastq_iter.py --- 
# 
# Filename: test_fastq_iter.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Fri Sep 16 18:03:27 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

# argument parser
import argparse
# system-specific parameters
# and function
import sys
# module for reading and writing FASTQ and QUAL format files as SeqRecord objects
from Bio.SeqIO import QualityIO
#
import os

def main(arguments):

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()
    if args.file_size == 0 and args.num_blocks == 0:
            sys.exit("Invalid argumet value 0 for --nblocks/--fsize flags!")

    # use float in order to be able to use exponential forms
    # in command line. Here I round to int for comparison
    num_blocks = int(args.num_blocks)
    file_size = int(args.file_size)

    current_file_index = int(1)
    i = int(0)
    
    if not os.path.exists(args.target_dir):
        os.makedirs(args.target_dir)
    
    # first segment
    current_file = open(os.path.join(args.target_dir, "%i.segment" % current_file_index), "w")

    # loops over blocks. Iterate over Fastq records. Not try to interpret
    # the quality string
    for title_line, seq_string, quality_string in QualityIO.FastqGeneralIterator(open(args.fastq_file, "r")):
    
        # split by block number      
        if (i >= num_blocks) and \
               (num_blocks != 0 ):
            current_file_index, \
            current_file, \
            i = get_new_segment(current_file_index, \
                                current_file, \
                                i, args.target_dir)

        # split by file size
        if (os.fstat(current_file.fileno()).st_size >= file_size) and \
               (file_size != 0):
            current_file_index, \
            current_file, \
            i = get_new_segment(current_file_index, \
                                current_file, \
                                i, args.target_dir)
            
        i +=1

        # raw fastq writting
        current_file.write("@%s\n%s\n+\n%s\n" % (title_line, seq_string, quality_string))
            
    return 0





def get_new_segment(current_file_index, current_file, i, target_dir):
    current_file_index +=1
    current_file = open(os.path.join(target_dir,"%i.segment" % current_file_index), "w")
    i = 0
    return current_file_index, current_file, i
    





def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Splits a fastq file into several segments of fixed size or fixed number of blocks (fastq sequences)", prog=sys.argv[0])
    parser.add_argument("--version", "-v", action="version", version="%(prog)s 0.1")
    parser.add_argument("--fastq", "-q", dest="fastq_file", required=True, help="Full path of a valid file in fastq format")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--fsize", "-s", dest="file_size", default=0, type=float, help="Maximum file size for each segment. The value must be an integer or exponential. The value 0 is excluded")
    group.add_argument("--nblocks", "-b", dest="num_blocks", default=0, type=float,  help="Maximum number of fastq blocks for each Segment. The value must be an integer or exponential. The value 0 is excluded")
    parser.add_argument("--target_dir", "-t", dest="target_dir", default=".", required=False, help="Destination directory for the segments. Default is current directory")
    
    args = parser.parse_args()
    
    return args




# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)




# 
# test_fastq_iter.py ends here
