#!/usr/bin/env python

# segr_filter.py --- 
# 
# Filename: segr_filter.py
# Description: 
# Author: Michele Vidotto
# Maintainer: 
# Created: Sun Sep 25 10:46:29 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
import sys
#
import argparse


def main(arguments):

    alleles = int(2)
    j = int(0)
    # minimum presence sum for parent replicates
    Pp = int(2)
    # minimum absence sum for parents replicates
    Pa = int(0)
    # minimum presence sum for all offsprings
    Op = int(4)
    # count clusters that satisfy the conditions
    clust_count = int(0)
    # counts the parental heterozygous biallelic loci
    bi_all_count = int(0)
    
    s = []

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()
    Pp = args.Pp
    Pa = args.Pa
    Op = args.Op

    # direct use bash input stream as open file
    for i, line in enumerate(sys.stdin):
        # print first line as header
        if i == 0:
            if line.strip() != "":
                print line.strip()
        else:
            if line.strip() != "":
                # get allele number
                allele_num = str2Int(line.split("\t")[1])
                # di if allele number is what expected
                if allele_num == alleles:
                    # get segregation pattern
                    seg_pattern = line.split("\t")[2]
                    # total time of presence/absence of tags
                    # for replicas of the parents and descendants
                    P1_sum, P2_sum, OFs_sum = allelesSums(seg_pattern)
                    # as tuple
                    s.append((P1_sum, P2_sum, OFs_sum, line.strip()))
                    j +=1
                
                    if j == alleles:
                        # test for alternative homozygous parents 
                        if ( (s[0][0] >= Pp and s[0][1] == Pa) and  (s[1][0] == Pa and s[1][1] >= Pp) ) or ( (s[0][0] == Pa and s[0][1] >= Pp) and (s[1][0] >= Pp and s[1][1] == Pa) ):
                            bi_all_count +=1
                            #print_candidates(alleles, s)
                            
                            # performs a test for all heterozygous offsprings
                            if ( s[0][2] >= Op and s[1][2] >= Op ):

                                clust_count +=1
                                print P1_sum, P2_sum, OFs_sum
                                print_candidates(alleles, s)

                        s = []
                        j = 0

    sys.stderr.write("Parental homozygous biallelic clusters = %i\n" % (bi_all_count))
    if bi_all_count != 0:
        rate = round( float(clust_count) / float(bi_all_count)*100, 2)
    else:
        rate = float(0)
    sys.stderr.write("With heterozygous offsprings = %i (%.2f %%)\n" % (clust_count, rate) )
    return 0





def diffStrings(str_1, str_2):
    """
    Compare 2 strings character by character to find polymorphisms.
    The string must have exactly the same length
    """
    for c1,c2 in zip(str_1, str_2):
        if c1 != c2:
            sys.stdout.write("*")
        else:
            sys.stdout.write(" ")
    return 0




def print_candidates(alleles, list):
    for n in range(alleles):
        print list[n][3]
    # for indentation
    sys.stdout.write("\t" * 4)
    diffStrings(list[0][3].split("\t")[3], list[1][3].split("\t")[3])
    print "\n"
    
    return 0





def allelesSums(vec):
    """
    Get sums of segregation patterns:
    0-2 P1 replies
    4-5 P2 replies
    6-12 offsprings
    """
    P1_sum = int(0)
    P2_sum = int(0)
    OFs_sum = int(0)
    
    for i in range(3):
   	P1_sum = P1_sum + str2Int(vec[i])

    for i in range(3,6):
    	P2_sum = P2_sum + str2Int(vec[i])
    
    for i in range(6,12): 
    	OFs_sum = OFs_sum + str2Int(vec[i])
    
    return P1_sum, P2_sum, OFs_sum



def str2Int(s):
    """
    Convert string to int if possible
    """
    try:
        ret = int(s)
    except ValueError:
        #Try float.
        ret = int(0)
    return ret




def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Filter for biallelic loci")
    parser.add_argument("--Pp", "-p", dest="Pp", required=False, type=int, default=2 , help="Minimum presence sum for parent replicates (2)")
    parser.add_argument("--Pa", "-a", dest="Pa", required=False, type=int, default=0 , help="Minimum absence sum for parents replicates (0)")
    parser.add_argument("--Op", "-o", dest="Op", required=False, type=int, default=5 , help="Minimum presence sum for all offsprings (5)")

    args = parser.parse_args()
    
    return args







# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)

# 
# segr_filter.py ends here
