#!/bin/sh

FILE_NAME=stursnp_frags_na.txt
FASTA_INTERMID=tmp.fasta
FASTA_FILE=stursnp_frags_na.fasta

bawk '!/^[$$,\#+]/ FNR>1 {
# escape first line
if (FNR>1)
{

split($0,a,"\t");

# for (i = 1; i <= NF; i++) 
#     {
# 	printf "%i\t%s\n", i, a[i];
#     }

printf "%s_%i\t%s\n", a[4], a[1], a[4];
}

}' $FILE_NAME | 
# sort lines
bsort |
# discard all but one of successive identical lines
uniq -c |
# remove number of occurrences that prefix lines
bawk '/[^[:space:]]/ {
split($0,a," ");
printf "%s\t%s\n", a[2], $2;
}' |
# make a fasta
tab2fasta > $FASTA_INTERMID;
# go to newline in fasta
fastatool sanitize $FASTA_INTERMID $FASTA_FILE;
rm -f $FASTA_INTERMID;









# awk -v genes_number=$GENES_NUMBER -v genes_discovered=$GENES_DISCOVERED -v transcripts_number=$TRANSCRIPTS_NUMBER -v transcripts_discovered=$TRANSCRIPTS_DISCOVERED '!/^[$$,\#+]/ { 
# x++

# if (x % 2 == 0)
# {
#     # work on even lines
#     split($0,a,"\t");
#     # eliminate first word from R write.table()
#     for (i = 2; i <= NF; i++) 
#     {
# 	printf "%s\t", a[i];
#     }
#     # rate of gene actually discovered
#     genes_dicovery_rate = 100*(genes_discovered/genes_number);
#     # rate of genes potentially discovered from parameters a of
#     # the non linear model fitting
#     max_genes_dicovery_rate = 100*(a[4]/genes_number);
#     # rate of transcript actually discovered
#     transcripts_discovery_rate = 100*(transcripts_discovered/transcripts_number);
#     # add values at the end of second line in $NL_MODEL_PARAMETERS_FILE
#     printf "%i\t%i\t%.2f\t%.2f\t%i\t%i\t%.2f\n", 
# 	genes_number,
# 	genes_discovered,
# 	genes_dicovery_rate, 
# 	max_genes_dicovery_rate, 
# 	transcripts_number,
# 	transcripts_discovered,
# 	transcripts_discovery_rate;
# }
    
# else
# {
#     # work on odd lines
#     split($0,a,"\t");
#     # print all elements
#     for (i = 1; i <= NF; i++) 
#     {
# 	printf "%s\t", a[i];
#     }
#     # add titles at the end of first line in $NL_MODEL_PARAMETERS_FILE
#     printf "total_genes\tgenes_discovered\tgenes_dicovery_rate\tmax_genes_dicovery_rate\ttotal_transcripts\ttranscripts_discovered\ttranscript_dicovery_rate\n";
# }

# }' $NL_MODEL_PARAMETERS_FILE
