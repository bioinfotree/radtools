#!/usr/bin/env python
# rsite_finder.py ---
# 
# Filename: rsite_finder.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Tue Sep 27 10:27:06 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
import sys
# Generator function to iterate over Fasta records (by biopython)
# return SeqRecord objects.
from  Bio.SeqIO import FastaIO



def main(arguments):
    # get python dict of fasta sequences
    for record in FastaIO.FastaIterator(sys.stdin):
        print r"SEQ_NAME: %s" %(record.id)
        print r"SEQ_LEN: %i" %(len(record.seq))
        print r"SEQ: %s" %(record.seq)
        print "---"
            
    return 0



# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)



# 
# rsite_finder.py ends here
