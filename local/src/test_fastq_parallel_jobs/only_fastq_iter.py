# test_fastq_iter.py --- 
# 
# Filename: test_fastq_iter.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Fri Sep 16 18:03:27 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

# argument parser
import argparse
# system-specific parameters
# and function
import sys
#
from Bio.SeqIO import QualityIO



def main(arguments):

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()

    for title_line, seq_string, quality_string in QualityIO.FastqGeneralIterator(open(args.fastq_file, "r")):
        pass
        #print title_line
        
    return 0


def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Get the first SIZE number of sequence from a fastq file and print them on std-out. Sequences are first indicized on a DB_FILE file, if the file does not exist.")
    parser.add_argument("--fastq", "-q", dest="fastq_file", required=True, help="Full path of a valid file in fastq format")
    # parser.add_argument("--size", "-s", dest="sample_size", default=10000, required=True, type=int, help="Number of sequences to be extracted")
    # parser.add_argument("--db_file", "-d", dest="db_file_name", default="gb_index.idx", required=False, help="Name of the SQLite3 index file generated. Default is gb_index.idx")
    args = parser.parse_args()
    
    return args




# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)




# 
# test_fastq_iter.py ends here
