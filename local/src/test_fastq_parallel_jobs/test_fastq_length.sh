#!/bin/bash
echo "Usage: sh script_name fastq.segment\n";
echo "Output: number of fastq sequence in fastq.segment\n";
/home/michele/bioinfotree/prj/radseq/local/src/fastx_toolkit-0.0.13/fastq_to_fasta -n -i $1 | fasta_count;
