#!/bin/sh


BARCODES_MAP=../sturgeons.pools.map
BARCODE_LENGTH=5


for i in sample_*.fq_*; do
bawk -v file=$i -v barcode_length=$BARCODE_LENGTH '!/^[$$,\#+]/ {
split($0,a,"\t");
# get tag from BARCODES_MAP file
tag = substr(file, 8, barcode_length);
# get current file extension
split(file,b,".");
ex = b[2];
# rebuild AN names
if ( match(a[2], /^AN/) != 0)
{
    split(a[2],c,"_")
    a[2] = c[1] "-" c[2] "_" c[3] "_" c[4]
}
# prepare instruction for renaming
if (a[1] == tag)
{
   printf "mv %s %s_%s.%s\n", file, a[2], tag, b[2];
}
# rename file
}' $BARCODES_MAP | bash;
done


# function string_slice {
#     STRING="$1"
#     declare -i LENGTH="${#STRING}"
#     declare -i START="$2"
#     declare -i END="$3"
#     if [ $START -lt 0 ]; then
#         START=$[ $LENGTH + $START ]
#     fi
#     if [ $END -le 0 ]; then
#         END=$[ $LENGTH + $END ]
#     fi
#     START=$[ $START + 1 ]
#     (echo "$STRING" | cut -c $START-$END) 2> /dev/null
# }
