#!/bin/sh

FILE_NAME=radtags_aln_tab.txt
CONTIGS_FASTA=CDNA3-4_11_2010_1_final_assembly.fasta
TAGS_FASTA=radtags.fasta

CONTIGS_COUNT=`fasta_count $CONTIGS_FASTA`
TAGS_COUNT=`fasta_count $TAGS_FASTA`

bawk -v contigs_count=$CONTIGS_COUNT -v tags_count=$TAGS_COUNT '

BEGIN { print "aligned_contigs\taligned_tags\tfrac_aligned_contigs\tfrac_aligned_tags" }

!/^[$$,\#+]/ {

split($0,a,"\t");

# for (i = 1; i <= NF; i++) 
#     {
# 	printf "%i\t%s\n", i, a[i];
#     }

tags = tags + a[3]

} END {

frac_tags = tags/tags_count*100
frac_contigs = NR/contigs_count*100

printf "%i\t%i\t%.2f\t%.2f\n", NR, tags, frac_contigs, frac_tags

} ' $FILE_NAME
