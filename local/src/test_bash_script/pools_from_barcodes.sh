#!/bin/sh

FILE_NAME=barcodes.txt

# creare rad pools table da file
bawk '

!/^[$$,\#+]/ {

split($0,a,"\t");

# for (i = 1; i <= NF; i++) 
#     {
# 	printf "%i\t%s\n", i, a[i];
#     }

split(a[3],b,",");

for (i in b)
  {
    if (b[i] != "-")
    {
       printf "%s_%s_%i\t%s\n", a[1], a[2], i, b[i];
    }
  }

}  ' $FILE_NAME
