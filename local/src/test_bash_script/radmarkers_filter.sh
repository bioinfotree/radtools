#!/bin/sh

FILE_NAME="SturSNP-AN.frags.3mis.3thres";
ALLELES_NUMBER=2;

# filter clusters for tag numbers
# print all tags of a clusters on the same line
# one line = one cluster

# only non blank line
bawk -v n=$ALLELES_NUMBER 'BEGIN {j=0;} !/^[$$,\#+]/ {

# print first line because is header
if (FNR == 1)
{
    for (i=0; i<n; i++)
    {
	printf "%s|", $0;
    }
    printf "\n";
}

split($0,a,"\t");

# alleles number
n = 2;
# filter by alleles number
if (a[2] == n)
{
    j++;
    printf "%s|", $0;
}

# prints newline every alleles-number lines
if (j == n)
{
  printf "\n";
  j = 0;
}

}' $FILE_NAME | ./segr_filter














# # only non blank line
# bawk '

# # must declare all parameters as local variables
# function alleles_sums(vec, b, num_pools, i, P1_sum, P2_sum, OFs_sum, ret) 
# {

#     num_pools = split(vec, b, "");

#     # for (i = 1; i <= num_pools; i++) 
#     # {
#     #   printf "%i\t%i\n", i, b[i];
#     # }

#     P1_sum = 0;
#     for (i = 1; i <= 3; i++) 
#     {
#     	P1_sum = P1_sum + b[i];
#     }

#     P2_sum = 0;
#     for (i = 4; i <= 6; i++) 
#     {
#     	P2_sum = P2_sum + b[i];
#     }


#     OFs_sum = 0;
#     for (i = 7; i <= 12; i++) 
#     {
#     	OFs_sum = OFs_sum + b[i];
#     }
#     # can return only one statement
#     return ret
# }


# BEGIN {} !/^[$$,\#+]/ {


# split($0,a,"|");

# # print first line because is header
# if (FNR == 1)
# {
#    print a[1];
# }
# else
# {
#     split(a[1],b,"\t");

#     alleles_sums(b[3]);

#     print a[1];
#     #printf "%i\t%i\t%i\n", ret[1], ret[2], ret[3]; 
# }
# }' 







# # only non blank line
# bawk 'BEGIN {} !/^[$$,\#+]/ {
# # print first line because is header
# if (FNR == 1)
# {
#    print $0;
# }

#     split($0,a,"\t");

#     # for (i = 1; i <= NF; i++) 
#     #     {
#     # 	printf "%i\t%s\n", i, a[i];
#     #     }


#     # alleles number
#     n = 2;
#     # filter by alleles number
#     if (a[2] == n)
#     {
    
#       num_pools = split(a[3], b, "")

#       # for (i = 1; i <= num_pools; i++) 
#       # {
#       #   printf "%i\t%i\n", i, b[i];
#       # }

#       P1_sum = 0;
#       for (i = 1; i <= 3; i++) 
#       {
# 	P1_sum = P1_sum + b[i];
#       } 

#       P2_sum = 0;
#       for (i = 4; i <= 6; i++) 
#       {
# 	P2_sum = P2_sum + b[i];
#       } 

#       OFs_sum = 0;
#       for (i = 7; i <= 12; i++) 
#       {
# 	OFs_sum = OFs_sum + b[i];
#       }


#       expr_1 = 0;
#       if (P1_sum >= 2 && P2_sum == 0 && OFs_sum >= 5 )
#       {
# 	expr_1 = 1;
#       }

#       expr_2 = 0;
#       if (P1_sum == 0 && P2_sum >= 2 && OFs_sum >= 5 )
#       {
# 	expr_2  = 1;
#       }

#       # if ( ( (expr_1 == 1 && j == 1 ) && (expr_2 == 1 && j == 2 ) ) || ( (expr_2 == 1 && j == 1 ) && (expr_1 == 1 && j == 2 ) ) ) 

#       if (expr_1 == 1 )
#       {
# 	print $0; 
# 	#printf "%i\t%i\t%i\t%i\n", j, P1_sum, P2_sum, OFs_sum;    
#       }
#       if (expr_2 == 1 )
#       {
# 	print $0; 
# 	#printf "%i\t%i\t%i\t%i\n", j, P1_sum, P2_sum, OFs_sum;    
#       }
#     }

#     # # prints newline every alleles-number lines
#     # if (j == n)
#     # {
#     #   printf "\n";
#     #   j = 0;
#     # }


# }' $FILE_NAME









# awk -v genes_number=$GENES_NUMBER -v genes_discovered=$GENES_DISCOVERED -v transcripts_number=$TRANSCRIPTS_NUMBER -v transcripts_discovered=$TRANSCRIPTS_DISCOVERED '!/^[$$,\#+]/ { 
# x++

# if (x % 2 == 0)
# {
#     # work on even lines
#     split($0,a,"\t");
#     # eliminate first word from R write.table()
#     for (i = 2; i <= NF; i++) 
#     {
# 	printf "%s\t", a[i];
#     }
#     # rate of gene actually discovered
#     genes_dicovery_rate = 100*(genes_discovered/genes_number);
#     # rate of genes potentially discovered from parameters a of
#     # the non linear model fitting
#     max_genes_dicovery_rate = 100*(a[4]/genes_number);
#     # rate of transcript actually discovered
#     transcripts_discovery_rate = 100*(transcripts_discovered/transcripts_number);
#     # add values at the end of second line in $NL_MODEL_PARAMETERS_FILE
#     printf "%i\t%i\t%.2f\t%.2f\t%i\t%i\t%.2f\n", 
# 	genes_number,
# 	genes_discovered,
# 	genes_dicovery_rate, 
# 	max_genes_dicovery_rate, 
# 	transcripts_number,
# 	transcripts_discovered,
# 	transcripts_discovery_rate;
# }
    
# else
# {
#     # work on odd lines
#     split($0,a,"\t");
#     # print all elements
#     for (i = 1; i <= NF; i++) 
#     {
# 	printf "%s\t", a[i];
#     }
#     # add titles at the end of first line in $NL_MODEL_PARAMETERS_FILE
#     printf "total_genes\tgenes_discovered\tgenes_dicovery_rate\tmax_genes_dicovery_rate\ttotal_transcripts\ttranscripts_discovered\ttranscript_dicovery_rate\n";
# }

# }' $NL_MODEL_PARAMETERS_FILE
