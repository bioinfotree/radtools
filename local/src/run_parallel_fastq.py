#!/usr/bin/env python

# run_parallel_fastq.py --- 
# 
# Filename: run_parallel_fastq.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Mon Sep 19 15:46:27 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

# argument parser
import argparse
# system-specific parameters
# and function
import sys
#
import os
#
from ruffus import *
#
import subprocess
#
import logging
#
import glob
#
import fastq_splitter




def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Divide a fastq file in segments, applies an operation on them in parallel, combine the results into a single file", prog=sys.argv[0])
    parser.add_argument("--version", "-m", action="version", version="%(prog)s 0.1")
    parser.add_argument("--fastq", "-q", dest="fastq_file", required=True, help="Full path of a valid file in fastq format")        
    parser.add_argument("--tmp_dir", "-t", dest="tmp_dir", required=False, default="tmp", help="Path to temporary directory where calculations take place")
    parser.add_argument("--out_file", "-o", dest="out_file", required=True, help="Name of the final output file")
    parser.add_argument("--jobs", "-j", dest="jobs", required=False, default=1, type=int, help="Specifies the number of jobs (operations) to run in parallel")
    parser.add_argument("--verbose", "-v", dest="verbose", required=False, default=False, action="store_true", help="Print more detailed messages for each additional verbose level. E.g. run_parallel_blast --verbose --verbose --verbose ... (or -vvv)")
    
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--fsize", "-s", dest="file_size", default=0, type=float, help="Maximum file size for each segment. The value must be an integer or exponential. The value 0 is excluded")
    group.add_argument("--nblocks", "-b", dest="num_blocks", default=0, type=float,  help="Maximum number of fastq blocks for each Segment. The value must be an integer or exponential. The value 0 is excluded")

    group_1 = parser.add_mutually_exclusive_group(required=False)
    group_1.add_argument("--flowchart", "-f", dest="flowchart", required=False, help="Print flowchart of the pipeline to FILE. Flowchart format depends on extension. Alternatives include ('.dot', '.jpg', '*.svg', '*.png' etc). Formats other than '.dot' require the dot program to be installed (http://www.graphviz.org/)")
    group_1.add_argument("--desc", "-d", dest="print_desc", required=False, default=False, action="store_true", help="Only print a trace (description) of the pipeline. The level of detail is set by --verbose.")

    args = parser.parse_args()
        
    return args




def run_cmd(cmd_str):
    """
    Throw exception if run command fails
    """
    process = subprocess.Popen(cmd_str, stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE, shell = True)
    stdout_str, stderr_str = process.communicate()
    if process.returncode != 0:
        raise Exception("Failed to run '%s'\n%s%sNon-zero exit status %s" %
                            (cmd_str, stdout_str, stderr_str, process.returncode))




# parse command line arguments
args = arg_parse()

tmp_dir = args.tmp_dir

# control over parameters
if (args.file_size == 0) and (args.num_blocks == 0):
    sys.exit("Invalid argumet value 0 for --nblocks/--fsize flags!")
# use float in order to be able to use exponential forms
# in command line. Here I round to int for comparison
num_blocks = int(args.num_blocks)
file_size = int(args.file_size)


logger = logging.getLogger(sys.argv[0])
# We are interesting in all messages
if args.verbose:
    logger.setLevel(logging.DEBUG)
    stderrhandler = logging.StreamHandler(sys.stderr)
    stderrhandler.setFormatter(logging.Formatter("    %(message)s"))
    stderrhandler.setLevel(logging.DEBUG)
    logger.addHandler(stderrhandler)



@follows(mkdir(tmp_dir))


@split(args.fastq_file, os.path.join(tmp_dir, "*.segment"))   
def splitFastq (seq_file, segments):
    """Split sequence file into
    as many fragments as appropriate
    depending on the size of original_fasta"""

    # clean up any segment files from previous runs before creating new one
    for i in glob.glob("*.segment"):
        os.unlink(i)

    command = str()

    # sets segmets dimension and split
    if file_size != 0:
        command = r"fastq_splitter.py --fastq %s --fsize %i --target_dir %s" \
                 % (args.fastq_file, file_size, tmp_dir)

    elif num_blocks != 0:
        command = r"fastq_splitter.py --fastq %s --nblocks %i --target_dir %s" \
                 % (args.fastq_file, num_blocks, tmp_dir)

    print "#",command

    # use fastq_splitter script
    fastq_splitter.main(command.split())

    return 0




# execute commands on the segmented file
@transform(splitFastq, suffix(".segment"), [".cleaned.fastq", ".cleaned.success"])
def runCommand_1(seqFile,  output_files):
    #
    result_file, flag_file = output_files

    script = "wc"

    # execute the script mess_blast_results.py
    command = r"%s -l %s > %s" %(script, seqFile, result_file)

    print command

    try:
        run_cmd(command)
    except:
        sys.exit(u'Could not executed %s' %(script))

    # "touch" flag file to indicate success
    flag_file_handle = open(flag_file, "w")
    flag_file_handle.close()

    return 0




# merge the results into a single file
@merge(runCommand_1, args.out_file)
def combineResults(result_and_flag_files, combined_result_file):
    """Combine results"""
    #
    output_file = open(combined_result_file, "w")
    for result_file, flag_file in result_and_flag_files:
        output_file.write(open(result_file).read())
        
    return 0




# print list of tasks
if args.print_desc == True:
    pipeline_printout(sys.stdout, [combineResults], verbose=args.verbose)

# print flowchart
elif args.flowchart:
    # use file extension for output format
    output_format = os.path.splitext(args.flowchart)[1][1:]
    pipeline_printout_graph (open(args.flowchart, "w"),
                             output_format,
                             [combineResults],
                             no_key_legend = True)

# run pipeline
else:
    pipeline_run([combineResults],  multiprocess=args.jobs,
                 logger=logger, verbose=args.verbose)






# # Is invoked when the module is executed
# if __name__ == "__main__":
#     # Explicitly pass command line arguments
#     # because required vor invoke main from
#     # other script
#     main(sys.argv)


# 
# run_parallel_fastq.py ends here
