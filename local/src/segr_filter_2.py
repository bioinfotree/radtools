#!/usr/bin/env python

# segr_filter_2.py --- 
# 
# Filename: segr_filter.py
# Description: 
# Author: Michele Vidotto
# Maintainer: 
# Created: Sun Sep 25 10:46:29 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
import sys
#
import argparse


def main(arguments):

    alleles = int(2)
    j = int(0)
    
    P1_start = int(1)
    P1_stop = int(2)

    P2_start = int(3)
    P2_stop = int(4)

    P3_start = int(5)
    P3_stop = int(12)

    P1_p = int(2)
    P1_a = int(0)

    P2_p = int(2)
    P2_a = int(0)

    P3_p = int(0)
    P3_a = int(0)
    
    clust_count = int(0)
    bi_all_count = int(0)

    s = []

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()
    P1_start = args.p1_pn[0]
    P1_stop = args.p1_pn[1]

    P2_start = args.p2_pn[0]
    P2_stop = args.p2_pn[1]


    if args.p3_pn:
        P3_start = args.p3_pn[0]
        P3_stop = args.p3_pn[1]

    P1_p = args.p1_thr[0]
    P1_a = args.p1_thr[1]

    P2_p = args.p2_thr[0]
    P2_a = args.p2_thr[1]

    if args.p3_thr:
        P3_p = args.p3_thr[0]
        P3_a = args.p3_thr[1]


    Op = int(0)

    # direct use bash input stream as open file
    for i, line in enumerate(sys.stdin):
        # print first line as header
        if i == 0:
            if line.strip() != "":
                print line.strip()
        else:
            if line.strip() != "":
                # get allele number
                allele_num = str2Int(line.split("\t")[1])
                # di if allele number is what expected
                if allele_num == alleles:
                    # get segregation pattern
                    seg_pattern = line.split("\t")[2]
                    # total time of presence/absence of tags
                    # for replicas of the parents and descendants
                    P1_sum, P2_sum, OFs_sum = allelesSums(seg_pattern, \
                                                          P1_start, \
                                                          P1_stop, \
                                                          P2_start, \
                                                          P2_stop, \
                                                          P3_start, \
                                                          P3_stop)

                    # as tuple
                    s.append((P1_sum, P2_sum, OFs_sum, line.strip()))
                    j +=1
                
                    if j == alleles:
                        # test for alternative homozygous parents 
                        if ( (s[0][0] >= P1_p and s[0][1] == P2_a) and  (s[1][0] == P1_a and s[1][1] >= P2_p) ) or ( (s[0][0] == P1_a and s[0][1] >= P2_p) and (s[1][0] >= P1_p and s[1][1] == P2_a) ):
                            bi_all_count +=1
                            
                            # performs a test for all heterozygous offsprings
                            if ( s[0][2] >= P3_p and s[1][2] >= P3_p ):

                                clust_count +=1
                                print_candidates(alleles, s)
                            
                        s = []
                        j = 0


    header = "Parental_homozygous_biallelic_clusters"
    values = "%i" % bi_all_count

    if args.p3_pn or args.p3_thr:
        if bi_all_count != 0:
            rate = round( float(clust_count) / float(bi_all_count)*100, 2)
        else:
            rate = float(0)
        header = header + "\tWith_heterozygous_offsprings\theterozygous_offsprings_%"
        values = values + "\t%i\t%.2f" % (clust_count, rate)

    sys.stderr.write("%s\n%s\n" % (header, values)) 

    return 0





def diffStrings(str_1, str_2):
    """
    Compare 2 strings character by character to find polymorphisms.
    The string must have exactly the same length
    """
    for c1,c2 in zip(str_1, str_2):
        if c1 != c2:
            sys.stdout.write("*")
        else:
            sys.stdout.write(" ")
    return 0






def print_candidates(alleles, list):
    for n in range(alleles):
        print list[n][3]
    # for indentation
    sys.stdout.write("\t" * 4)
    diffStrings(list[0][3].split("\t")[3], list[1][3].split("\t")[3])
    print "\n"
    
    return 0




# assume contiguous column
def allelesSums(vec, \
                P1_start, \
                P1_stop, \
                P2_start, \
                P2_stop, \
                P3_start, \
                P3_stop):
    """
    Get sums of segregation patterns:
    0-2 P1 replies
    4-5 P2 replies
    6-12 offsprings
    """
    P1_sum = int(0)
    P2_sum = int(0)
    OFs_sum = int(0)
    
    for i in range(P1_start-1, P1_stop):
   	P1_sum = P1_sum + str2Int(vec[i])

    for i in range(P2_start-1, P2_stop):
    	P2_sum = P2_sum + str2Int(vec[i])
    
    for i in range(P3_start-1, P3_stop): 
    	OFs_sum = OFs_sum + str2Int(vec[i])
    
    return P1_sum, P2_sum, OFs_sum







def str2Int(s):
    """
    Convert string to int if possible
    """
    try:
        ret = int(s)
    except ValueError:
        #Try float.
        ret = int(0)
    return ret







def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="\
    Filter RADmarkers output from RADtools package. \
    Look for patterns of presence-absence of tags (alleles) in 2-allelic clusters (loci), in different pools. \
    Scans the field SegPattern in up to 3 groups of pools. \
    Groups are composed of adjacent pools, defined in the header of the *.markers file. \
    Group 1 and group 2 are compared for the presence of alternative alleles at the same 2-allelic locus. \
    Group 3 must contain both alleles at the same 2-allelic locus. \
    Each group is characterized by start-end interval (>1), with respect to the order of the pools, as defined in the header. \
    Each group is characterized by a minimum present number of alleles in order to define the allele present in the group and a number to define its absence in the group. \
    ", prog=sys.argv[0], epilog="michele.vidotto@gmail.com")
    parser.add_argument("--p1_pn", "-a", dest="p1_pn", required=True, type=int, nargs=2, help="[start end] positions to define interval of group 1 (>1). Population 1 or parent 1.")
    parser.add_argument("--p2_pn", "-b", dest="p2_pn", required=True, type=int, nargs=2, help="[start end] positions to define interval of group 2 (>1). Population 2 or parent 2.")
    parser.add_argument("--p3_pn", "-c", dest="p3_pn", required=False, type=int, nargs=2, help="[start end] positions to define interval of group 3 (>1). Offsprings.")
    parser.add_argument("--p1_thr", "-l", dest="p1_thr", required=True, type=int, nargs=2 , help="[present_num absent_num] in order to define allele present or absent in the group 1.")
    parser.add_argument("--p2_thr", "-m", dest="p2_thr", required=True, type=int, nargs=2 , help="[present_num absent_num] in order to define allele present or absent in the group 2.")
    parser.add_argument("--p3_thr", "-n", dest="p3_thr", required=False, type=int, nargs=2 , help="[present_num absent_num] in order to define allele present or absent in the group 3.")

    args = parser.parse_args()
    
    return args







# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)

# 
# segr_filter_2.py ends here


# ./segr_filter_2 --p1_pn 1 2 --p2_pn 3  4 --p1_thr 2 0 --p2_thr 2 0 < stursnps_pops_full.markers > persicus_gualdenstaedtii_nord_caspian.markers

# ./segr_filter_2 --p1_pn 7 9 --p2_pn 10  12 --p1_thr 2 0 --p2_thr 2 0 < stursnps_pops_full.markers > baeri_lena_vs_ob.markers

# ./segr_filter_2 --p1_pn 3 4 --p2_pn 5  6 --p1_thr 2 0 --p2_thr 2 0 < stursnps_pops_full.markers > gueldenstaedtii_azov_vs_caspian.markers

# ./segr_filter_2 --p1_pn 1 3 --p2_pn 4 6  --p3_pn 7 12  --p1_thr 2 0 --p2_thr 2 0 --p3_thr 3 0 < naccari_family_full.markers > op3_naccari_family_2allelic_off.markers 
