#!/usr/bin/env python
# dummy_qual_from_fasta.py --- 
# 
# Filename: dummy_qual_from_fasta.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Wed Aug 31 13:30:37 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# You coul add generation of an error profile depending on sequencing technology
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:


# argument parser
import argparse
# system-specific parameters
# and function
import sys
# implements pseudo-random number
# generators for various distributions.
import random
# generator function to iterate over Fasta records (as SeqRecord objects)
from Bio.SeqIO import FastaIO
# class to write QUAL format files (using PHRED quality scores)
# consists of 4 elements:
#
# 1) initialization of the class.
#
# 2) writing header
# 
# 3) writing several records
# 
# 4) writing footer
from Bio.SeqIO.QualityIO import QualPhredWriter



# TODO:
#     add generation of an error profile depending on sequencing technology
#     try to see at meren-pommunity-06fd34f

def main(arguments):

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()

    min_start = int(0)
    max_end = int(50)

    if args.start_value < min_start:
        sys.exit("Start value below %i! Stop!" % min_start)
    if args.end_value > max_end:
        sys.exit("End value higher then %i! Stop!" % max_end)
    
    # open handle: file or stdout
    out_file_handle = sys.stdout
    if args.out_file != None:
            out_file_handle = open(args.out_file, 'w')

    # Create a QUAL writer. 

    # Arguments: 
    #  - handle - Handle to an output file, e.g. as returned 
    #             by open(filename, "w") 
    #  - wrap   - Optional line length used to wrap sequence lines. 
    #             Defaults to wrapping the sequence at 60 characters 
    #             Use zero (or None) for no wrapping, giving a single 
    #             long line for the sequence. 
    #  - record2title - Optional function to return the text to be 
    #             used for the title line of each record.  By default 
    #             a combination of the record.id and record.description 
    #             is used.  If the record.description starts with the 
    #             record.id, then just the record.description is used. 

    # The record2title argument is present for consistency with the 
    # Bio.SeqIO.FastaIO writer class. 
    qual_phred_writer = QualPhredWriter(out_file_handle, wrap=args.wrap)
    
    # It is intended for sequential file formats with an (optional) 
    # header, repeated records, and an (optional) footer.
    
    # The user may simply call the write_file() method and be done. 
   
    # However, they may also call the write_header(), followed 
    # by multiple calls to write_record() and/or write_records() 
    # followed finally by write_footer(). 

    # Users must call write_header() and write_footer() even when 
    # the file format concerned doesn't have a header or footer. 
    # This is to try and make life as easy as possible when 
    # switching the output format. 
    
    # Note that write_header() cannot require any assumptions about 
    # the number of records.
    qual_phred_writer.write_header()

    for seq_record in FastaIO.FastaIterator(open(args.fasta_file, "r")):
        phred_scores = []

        # chooses a int random number between start and end.
        # The maximum allowed is 50 (maximum Phred quality)
        for base in seq_record:
            phred_scores.append(random.randint(args.start_value, args.end_value))
        # add qualities to SeqRecords obj
        seq_record.letter_annotations["phred_quality"] = phred_scores
        # multiple calls to write_record()
        qual_phred_writer.write_record(seq_record)

    # call write_footer()
    qual_phred_writer.write_footer()
    
    out_file_handle.close()
        
    return 0





def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Given a fasta file makes a file in Phred quality format, giving each base a random value between [--start] and [--end]. Results are directed to std-out by default.")
    parser.add_argument("--fasta", "-f", dest="fasta_file", required=True, help="Full path of a valid file in fasta format")
    parser.add_argument("--out", "-o", dest="out_file", required=False, help="Name of output file (Phred fasta quality)")
    parser.add_argument("--start", "-s", dest="start_value", default=0, required=False, type=int, help="Start value in the range of Phred quality (0-50)")
    parser.add_argument("--end", "-e", dest="end_value", default=50, required=False, type=int, help="End value in the range of Phred quality (0-50)")
    parser.add_argument("--wrap", "-w", dest="wrap", required=False, type=int, default=60, help="Optional line length used to wrap sequence lines. Defaults to wrapping the sequence at 60 characters. Use zero for no wrapping, giving a single long line for the sequence.")

    args = parser.parse_args()
    
    return args




# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)


# 
# dummy_qual_from_fasta.py ends here
