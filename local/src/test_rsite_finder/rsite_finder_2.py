from Tkinter import *
from tkMessageBox import * 
from tkFileDialog import *

#An example library of restriction enzymes and their recognition sites
site_enz= {'GAATTC':'EcoRI','GGATTC':'BamHI','AAGCTT':'HindIII',
           'CCCGGG':'Smal','GATATC':'EcoRV'}

class RestrictionSites (object):
    """A class to find splice sites within an RNA sequence chosen by the user"""

    
    def __init__(self, master):

        self.master = master
        self.frame = Frame(master)
        self.frame.pack()
        self.menubar = Menu(master)

        # selecting the image to use
        self.image = PhotoImage(file="")

        # create a canvas that fits the image used
        self.canvas = Canvas(self.frame,
                             width=self.image.width(),
                             height=self.image.height())
        # aligning the image to the top left corner of the canvas
        self.canvas.create_image(0,0, anchor=NW, image=self.image)
        self.canvas.pack()
        # now I want to add a button underneath, cannot but canvas and
        # button on the same frame so need to create a new frame for button
        self.button_panel = Frame(self.frame)
        self.button_panel.pack(side=TOP)

        # sets the width and position of the start button
        self.start_button = Button(self.button_panel,
                                   text="Click to upload a single stranded DNA sequence 5'-3'", width=100, command=self.startbutton)
        self.start_button.pack()

    def startbutton (self):
        self.box2()
        
    def box2 (self):

        
        #add a text area 80 characters wide and 20 characters high
        self.text = Text(self.frame, width=80, height= 20)
        self.text.pack()

        #underneath the text area add a label and left-justify its text
        self.label= Label(self.frame, text="Please select a file containing a 5' to 3' DNA sequence. ")
        self.label.pack(side=TOP, anchor=W)

        #Cant mix packa nd grid in the same frame, so
        #underneath the label add another Frame to hold the buttons
        self.button_panel = Frame (self.frame)
        self.button_panel.pack (side=TOP)

        #Put the check button in the left part of the frame
        self.check_button = Button(self.button_panel, text="Check", width=30,
                                   command=self.check)
        self.check_button.grid (row=0, column=0, padx=20)

        #Put the translate butotn in the right part of the Frame and grey it out
        self.find_button= Button(self.button_panel, text = "Find Restriction Sites", width=30,
                                command=self.FindSite)
        self.find_button.grid (row=0, column=1, padx=20)

        #Create a pulldown menu

                                 
        self.filemenu = Menu(self.menubar,tearoff=0)
        #add menu items
        self.filemenu.add_command(label="Open", command=self.ChooseFile)
        self.filemenu.add_command(label="Exit", command=self.bye)
        #add the menu to the menu bar
        self.menubar.add_cascade(label="File", menu=self.filemenu)

        #display the menu
        master.config(menu=self.menubar)
        

    def bye(self):
        #remove the window from the screen when they click bye
        self.master.withdraw()
        #Stop the event handling loop and garbage collect
        self.master.quit()

    def key_pressed(self, event):
        # strange syntax required by Tcl
        sequence = self.text.get(1.0,END).strip()
        # note that we receive the event BEFORE it has been processed
        if event.keysym=="BackSpace" and len(sequence)<=1:
            self.check_button.config(state=DISABLED)
            self.find_button.config(state=DISABLED)
        else:
            self.check_button.config(state=NORMAL)

    def check(self):
        # strange syntax required by Tcl
        sequence = self.text.get(1.0,END).strip()
        
        # code should now validate the sequence
        while True:
            
            self.file=self.FindFile()
            self.file=self.file.upper()

            #Only these bases allowed
            good_bases = "ATCG"

            temp_sequence = ''

            for base in self.file:
                if base in good_bases:
                    temp_sequence += base

            self.file = temp_sequence

            #Flag for error- assume OK at the start.
            bad_input = False

            #Check each base.
            for base in self.file:

                if base in good_bases:
                    #Ok so far, check next base.
                   continue
                else:
                    #Not an RNA base, so flag the error.
                    bad_input= True
                    #Give the user the bad news
                    print 'Sorry - that is not a DNA sequence, please try again.'
                    #break from the 'for' loop
                    break
            #By this stage the sequence has been checked.
            #If all is awell, break form the 'while' loop.
            if bad_input ==False:
                break
            #Otherwise the 'while' loop will go back for re-input.

        return self.file
    # if it is OK, the translate button should be enabled
        self.find_button.config(state=NORMAL)
            
    def FindSite (self):
        """This function checks if there is a restriction site in the DNA"""
        if site_enz.has_key(site)==False:

            return '?'
        
        return site_enz.get(site)
        
    
    def ChooseFile (self):
        #Save as dialog
        filename = askopenfilename(parent=root,initialdir="/",title='Please select a directory')
        #When I need the file I refer to this bit
        my_file=open(filename,'r')
        temp = my_file.read().upper()
        my_file.close()

        my_sequence=''
        for base in temp:
            if base in 'AUGC':
                my_sequence += base
        self.tect.insert (INSERT, my_sequence)

    def openfile(self):

        filename = askopenfilename(parent=root,initialdir="/",title='Please select a directory')
        my_file=open(filename,'r')
        temp=my_file.readline()
        temp=my_file.read().upper()
        my_file.close()

        my_sequence=''
        for base in temp:
            if base in 'AUGC':
                my_sequence += base

        self.text.insert(INSERT, my_sequence)

    
    def find_list_site( sequence, init_pos, restriction_site):
        """find list of sites in file"""

#function to find six-base restriction sites in a DNA sequence
#self.findfile is a string containing the DNA sequence
#init_pos is the position to start searching from (to start from the beginning, set it to 0)
#restriction_site (self.find_site) is a string containing the sequence sought....
        #but i need to get the site from the dictionary
#the function returns a list of positions where the restriction site was found
#if none are found then the list will be empty

#self.FindFile is the string that you read (and filter) from the file.
#restriction_site (self.find_site) is the sequence of one of the sites in your dictionary.
#For example for EcoRI, restriction_site would be 'GAATTC'.

#You would need to run the function for each restriction site in your dictionary.
        
        list_of_sites=[]

        end = len(sequence)-6

        for pointer in range(init_pos, end):
            sixer = self.FindFile[pointer:pointer+6]
            if sixer==GAAATC:
                list_of_sites.append(pointer)

            print list_of_sites
            return list_of_sites


#Main Program for opening window
root= Tk()
root.title("Restriction Site finder")
#Set this class to be an objecy
app=RestrictionSites(root)
root.mainloop
