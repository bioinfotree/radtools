#!/usr/bin/env python
# joins_rsites-aln.py --- 
# 
# Filename: joins_rsites-aln.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Wed Sep 28 17:47:01 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

# argument parser
import argparse
# system-specific parameters
# and function
import sys


def main(arguments):

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()

    first = {}
    for line in open(args.tab_files[0], "r"):
        seq_id = line.strip().split()[0]
        # escape header
        if  seq_id[0] != "#":
            first[seq_id] = line.strip()

    sys.stdout.write("# sequence_name\tsequence_length\taligned_reads\tnot_aligned_reads\trestriction_sites_count\trecognition_site_starting_points\n")

    rest_file_handle = open(args.rest_file,"w")
    
    for file_name in args.tab_files[1:]:
        count = int(0)
        for line in open(file_name, "r"):
            seq_id = line.strip().split()[0]
            # escape sequence_length in second file
            rest_line = line.strip().split()[2:]
            # escape header
            if  seq_id[0] != "#":
                if first.has_key(seq_id):
                    count +=1
                    sys.stdout.write("%s\t%s\n" %(first[seq_id], "\t".join(rest_line)))
                else:
                    rest_file_handle.write("%s\n" % line.strip())
                    
        sys.stderr.write("common hit: %i\n" % count)

  
    return 0









def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="", prog=sys.argv[0])
    parser.add_argument("--version", "-v", action="version", version="%(prog)s 0.1")
    parser.add_argument("--tabs", "-t", dest="tab_files", required=True, nargs=2  , help="tabs file to be substracted")
    parser.add_argument("--rest_file", "-r", dest="rest_file", required=False, default="rest_tab.txt", help="rest of minus")
    
    args = parser.parse_args()
    
    return args




# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)



# 
# joins_rsites-aln.py ends here
