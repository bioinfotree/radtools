#!/usr/bin/env python
# search_reads.py --- 
# 
# Filename: search_reads.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Wed Sep 28 15:47:54 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
import sys
#
import argparse
#
import os


def main(arguments):

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()

    common = {}
    for line in args.common:
        common[line.strip().split()[1]] = 1


    if args.target_dir != "":
        for filename in os.listdir(args.target_dir):
            if os.path.splitext(filename)[1][1:] == r"reads":
                abs_filename = os.path.join(args.target_dir,filename)
                #common_count = count_reads(abs_filename, common)
                common_count = find_reads(abs_filename, common, args)


    if args.target_file != "":
        #common_count = count_reads(args.target_file, common)
        common_count = find_reads(args.target_file, common, args)
    
    return 0



def count_reads(file, common):
    common_count = 0
    print file
    for line in open(file, "r"):
        # slicing the first 11 characters
        read = line.strip().split()[0][11:]
        if common.has_key(read):
            common_count += 1
            print line
    print common_count
    return common_count


def find_reads(file, common, args):
    common_count = 0
    sys.stdout.write("%s\t" % os.path.basename(file))
    for line in open(file, "r"):
        for string in common.keys():
            # Return the lowest index in the string where substring sub is found,
            find = line.find(string)
            if find != -1:
                common_count += 1
                sys.stdout.write("%i\t" % find)
                if args.print_seq == True:
                    print line.strip()
    sys.stdout.write("\n%i\n" % common_count)
    return common_count




def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--common", "-c", dest="common", required=True, type=file, help="")
    parser.add_argument("--print_seq", "-p", dest="print_seq", required=False, action="store_true", default=False, help="")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--target_dir", "-d", dest="target_dir", required=False, default="", help="")
    group.add_argument("--target_file", "-f", dest="target_file", required=False, default="", help="")

    args = parser.parse_args()
    
    return args



# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)



# 
# search_reads.py ends here
